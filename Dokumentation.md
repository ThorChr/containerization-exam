# Dokumentation

Følgende er en guide for hvordan man får projektet op og køre.

Jeg går ud fra at Docker allerede er installeret på ens maskine, hvis ikke, installer det.

## Step-by-step

Guiden er en step-by-step basis, der gør det meget tydeligt hvilke kommandoer der skal blive udført. Udfør dem i den rækkefølge de bliver nævnt i.

### Klon Repo

Til at starte med, skal vi have klonet dette repository ned. 

For at gøre det, åben en Terminal hvor end man nu vil. Jeg gjorde det på mit skrivebord. I ens Terminal skriver man så:

```console
git clone https://gitlab.com/ThorChr/containerization-exam
```

### Naviger til Repo

Næste skridt vil så være at navigere til ens repository. Dette gøres ved at bruge følgende kommando:

```console
cd .\containerization-exam\
```

### Byg Frontend Image

Nu skal vi så have bygget et image til vores frontend applikation. Dette kræver at man navigere til den rette mappe, og udføre en kommando. Skriv derfor følgende to kommandoer:

```console
cd .\project\frontend\
docker build -t exam_frontend .
```

### Byg Backend Image

Nu skal vi have bygget et image til backend delen, altså vores API. Naviger derfor hen til den korrekte mappe, og udfør en bestemt kommando. Brug følgende:

```console
cd ..\backend\
docker build -t exam_backend .
```

### Initializer Swarm

Nu skal vi så have initializet vores swarm. Brug derfor følgende kommandoer:

```console
cd ..
docker swarm init
```

### Deploy Stack på Swarm

Nu har man så oprettet en swarm, og vi har alle de nødvendige elementer klar. Så skal vi bare have deployed vores service på vores swarm. Dette gøres ved følgende kommando:

```console
docker stack deploy -c docker-compose.yaml examStack
```

### Tjek Status på Service

Det kan være en god ide lige at se status på ens service, for også lige at se hvilke ports der bliver brugt. Brug derfor følgende kommando:

```console
docker service ls
```

### Tilgå Siden

Nu er ens service så oppe og køre. Det næste er så at tilgå den. Åben derfor din web browser, og naviger til ```localhost:8080```. Nu skulle du gerne se at siden er oppe.