# Dokumentation

## Pre-Docker

Til at starte med, så kiggede jeg koden igennem på GitHub. Forsøgte at finde ud af hvad det var for nogle projekter, og om der var nogle configurations filer man skulle være opmærksom på. Efter noget tid fandt jeg så ud af at frontend er en ```Vue3``` app der bliver lavet med Vite. Derudover så er backend delen en ```Express.js``` app der håndtere alle de forskellige requests den modtager. Yderligere fandt jeg så ud af hvilke navn databasen burde have (er klar over at man selv kunne bestemme dette, men tænker det er en god ide at følge hvad der står i koden, så vidt muligt). Og så fandt jeg også ud af at det ikke kun er en MySQL server, men direkte en MariaDB database der bliver gjort brug af. 

Til sidst, før jeg så gik videre, så tjekkede jeg hvilke ports de forskellige thing vil ende med at gøre brug af. Jeg fandt frem til at:

- Vue3: ```8080```
- Express.js: ```3000```
- MariaDB: ```3306```

Hvilke er ret gode informationer af have, når man senere via Docker skal expose bestemte porte.

## Docker Opsætning

Ret hurtigt fandt jeg frem til at jeg ville skulle gøre brug af en compose fil, som så skulle "opsætte" min swarm. Der er noget forskel mellem ```docker-compose``` og ```docker swarm```, og eftersom jeg er mere vant til at bruge docker-compose, så skulle jeg lige være klar over de forskelle her. En af de primære er, at en docker swarm kræver at ens ```images``` allerede er bygget, den kan ikke selv bygge dem.

Hvilket betød for mig, at jeg skulle ind og oprette et image for både ```frontend``` og ```backend```.

Derefter ville jeg så kunne samle det hele i én ```docker-compose.yaml``` fil, der ville kunne bruges til at sætte ens swarm op.

## Repository

Det første skridt i faktisk at arbejde med projektet, var at hente repositoriet ned på min lokale maskine.

Jeg gør som jeg altid gør, og brugte så Terminal til at finde et sted jeg kunne placere det, via følgende commando:

```bash
cd E:\Repositories\pbasoftwarecontainerization\
mkdir exam
cd exam
mkdir solution
cd solution
git clone https://github.com/ucldk/cl23e-exam-project
cd cl23e-exam-project
ls
```

Nu havde jeg så fået hentet projektet ned, og jeg har også en oversigt i min Terminal over hvilke filer der er i rodmappen.

## Frontend

Jeg endte med at arbejde med Frontend til at starte med. Som tidligere nævnt, så kræver en docker swarm at man peger på direkte images. Hvilket betyder at jeg så skal have lavet et image til frontend, hvilket var perfekt, da det også gjorde det meget let og hurtigt at teste forskellige ting, og se hvad der virkede og ikke virkede, og om der var nogle ting der skulle ændres.

Jeg kan se via ```readme.md``` at der er nogle forskellige commands der kan blive kørt. Der står dog en command for at køre det via development. Dog ønskede jeg ikke at gøre dette, jeg ville gerne køre det som production. ```package.json``` angiver ikke nogle direkte måder for at gøre det på, så jeg tænkte "Kan jeg potentielt gøre brug af ```npm run build``` og så flytte resultat af det, over i en Nginx container, og hoste det således?" Det var så den tilgang jeg endte med at tage.

Jeg foretog en hurtig Google søgning efter *"vue3 build to nginx"* og tog det første resultat. Det var ikke præcist hvad jeg ledte efter, men den info den havde kunne jeg kombinere med ting jeg tidligere havde lavet i undervisningen.

Resultatet var altså en ```Dockerfile``` der er delt op i 2 steps:

1. Build.
2. Host.

Under mit build step, sørgede jeg for at flytte filerne hen hvor de skulle ligge, og jeg sørgede for at installere de forskellige dependencies.

Under mit host step, sørgede jeg for at fjerne den default nginx configurations fil, og så tilføje en ny en. En der passer bedre til det projekt jeg laver. Derefter bliver den så flyttet ind hvor den skal være, og så flytter jeg også lige på filerne fra vores build step, over i vores nginx. Derefter så bliver ngixn ellers bare startet op, og så er den Dockerfile done.

Jeg fik jo nævnt at jeg lavede min egen nginx configuration fil, altså ```nginx.conf```. Begrundelsen til det, er så vi ikke løber ind i problemer med selve vores SPA, og hvordan at Nginx forsøger at navigere med den. Uden vores fiks, risikere vi at man får en 404 hvis man er på en underside, og forsøger at refresh siden.

Dog så er vi ikke helt klar endnu. Der var nemlig 3 fejl med koden:

1. Manglende slashes for src properties i html.
2. Manglede billede for produkter der bliver oprettet via API'en.
3. Appen har ikke mulighed for at håndtere en API, når den er deployed i Production mode.

### Manglende slashes for src properties i html

Dette var et problem der påvirkede 2 sider (eller komponenter), nemlig ```AboutView.vue``` og ```BrandCarousel.vue```. Måden jeg fiksede det var lidt primitivt, men da jeg først bagefter vidste hvilke ting der var påvirket, så var mit fiks følgende: Jeg åbnede alle .vue filer op, lavede en CTRL+F efter "assets", og kiggede så om der stod "/assets" eller bare "assets". Hvis det var den sidste, så blev det rettet, fordi det er forkert.

Efter jeg havde gået alle filerne igennem så var den fejl fikset.

### Manglede billede for produkter der bliver oprettet via API'en.

Når man opretter et produkt via API'ens endpoint ```/test``` så bliver det item oprettet med en thumbnail værdi af: ```/assets/ladida.png```. Dette billede eksisterer ikke. Derudover så peger det også et forkert sted hen, der burde stå ```/assets/img/ladida.png```, men whatever. Jeg fiksede dette ved at tage et eget billede, omdøbe dette til *ladida.png* og så placerede jeg det i mappen "assets".

### Appen har ikke mulighed for at håndtere en API, når den er deployed i Production mode.

Denne fejl kommer fra filen ```main.js```. Fejlen her, er noget manglende kode. Der er en dictionary der bliver erklæret, og denne indeholder by default, kun én værdi, nemlig "developement". Jeg vil dog publish som production, så dette skal fikses. Jeg fiksede det ved at tilføje endnu et item, og kaldte det "production".

### Oprettelse af image

Nu havde jeg så en Dockerfile der beskrev hele processen for at sætte dette projekt op. Herefter skulle jeg så få det oprettet, hvilket jeg gjorde ved følgende kommando:

```bash
docker build -t exam_frontend .
```

Og med den kommando udført, så havde jeg min image som jeg kan gøre brug af, og frontend delen var færdig.

## Backend

Ligesom med frontend, så skulle man her også oprette et image, og processen var meget ens. Dog den her gang, bliver der ikke gjort brug af nginx, da der er en kommando i ```package.json``` der er oprettet der hedder ```start```. Jeg endte med at gøre brug af den, fordi jeg gik ud fra at det er tilsvarende til production, da der nemlig også er en "dev" kommando lidt længere oppe.

Men yes, jeg endte så med at navigere hen til den mappe. Derinde, lavede jeg så en Dockerfile. Denne fil blev så fyldt ud med de nødvendige kommandoer for at bygge en ```Express.js``` API. Den er meget basal, men der skal heller ikke så meget til den.

Der var ikke nogle fejl / mangler i dennes kode, derfor var der ikke flere ekstra ting at gøre, udover nu at bygge vores image:

```bash
docker build -t exam_backend .
```

## Database

Vi fandt tidligere ud af at der skal være en database as well, og at det er en mariadb database. Heldigvis behøvede jeg ikke at lave noget ekstra image her, jeg kunne bare gøre brug af et der er hosted via ```hub.docker.com``` og ændre på nogle environment variables.

## Docker Compose Filen

Så nåede man hen til den sidste del, hvilket handler om at inkludere det hele sammen i én ```docker-compose.yaml``` så man kan bruge den til swarm.

Der er ikke så meget at sige her. Det er egentligt bare en samling af alt det tidligere arbejde der skete her, men jeg kan da lave en kort gennemgang af det.

Jeg valgte at gøre brug af den seneste version, så jeg så version 3.8 (hvilket jeg fandt ud af var den seneste ved at Google).

Det næste der så sker er at vi definere vores services. Der er et total af 3 services, hvilket er:

1. frontend
2. backend
3. db

### Frontend Service

Den her er meget straight-forward. Starter ud med at sige hvilket image der skal bruges. Det næste der bliver beskrevet, er hvordan man skal deploy den service via docker swarm, hvilket egentligt bare siger "Jeg vil have 3 kopier, og jeg vil have at en kopi genstarter når der sker en fejl." Så til sidst håndtere jeg så ports. Her siger jeg bare "Al' trafik der kommer til min host maskine på port 8080 skal blive navigeret videre hen til port 80 i denne service". Grunden til at det er port 80, er fordi vi hoster det med nginx.

Med det sagt, så er hele frontend service blevet beskrevet. Meget simpel.

### Backend Service

Den her er lidt længere, dog følger den nogle af de samme ting. Stadigt et image, stadigt angivelse af hvordan man deployer den, og stadigt port angivelse (her bruges 3000 fordi det er præcist hvad vores Express.js API bruger).

Der hvor det er anderledes er at nu angiver vi environment varibles og vi angiver en command til at blive kørt.

Alle de forskellige enrivonment variables, bruges til at sætte databasen forbindelsen op. Hvad de forskellige variabler skal hedde kommer direkte fra filen ```database.js``` der eksisterer under backend/config.

Dernæst angiver vi commands der skal blive kørt. Begrundelsen til det, er at vi vil sikre os at databasen har de migrations den skal have. En migration er en beskrivelse af nogle kommandoer der skal udføres på databasen, fx at en bestemt tabel skal eksistere og se ud på en bestemt måde. Vi kører så 2 commands, en til at oprette de nødvendige tabeller, og en til at fylde den med en masse informationer.

Og det er så selve vores backend service.

### Db Service

Præcist som alle de andre services, så har man image og deploy.

Her angiver vi dog også nogle environment variables, men vi angiver også volume til vores service.

De forskellige environment variables der bruges, siger egentligt bare "Koden til root brugeren skal være [whatever man så har skrevet]" og den næste siger "Når denne service starter, skal vi sikre os at der eksisterer en database der hedder [whatever man så har skrevet]".

Volume bruges til at sikre os at vi har noget persistent storage. Det ville ikke være særlig godt hvis at databasen mistede al sin data, hver gang den genstartede. Derfor bruger vi volumes. 

#### Problem med volume

Jeg havde et problem i starten, hvor at jeg havde lavet min database service dog uden at angive en database der skulle oprettes. Da jeg så ændrede i den og netop angav en bestemt database der skulle eksistere, så virkede det stadigt ikke, og den database blev aldrig oprettet. Jeg fandt så ud af, at det var fordi at en database ikke bliver oprettet via det environment flag, hvis at det volume som bruges ikke er tomt. 

Løsningen her var så at slette det tidligere (alligevel worthless) volume der blev brugt, og så starte service op igen, og så virkede det.

### Volumes

Her skal det siges, at jeg *ikke* gør brug af et netværks volume. Hvis at jeg ønskede at deploy min stack på tværs af flere enheder, ville det være en ting man skulle gøre brug af. Dog eftersom det bliver kørt på én maskine, så har jeg ikke gjort det. Hvis man dog ville angive det, så ville man kunne sætte det op, i den nederste sektion i ens docker-compose.yaml fil hvor der står "volumes". Her står den volume der bliver brugt, og her har man mulighed for at angive den som en nfs hvis man ønskede.

Det kan dog godt være at det kræver noget ekstra opsætning for selve ens enhed før at man kan gøre helt brug af det.

## Kørsel

For at køre projektet, skal man sikre sig et par ting:

1. Frontend image eksisterer og har navnet ```exam_frontend```.
2. Backend image eksisterer og har navnet ```exam_backend```.

Hvis disse ikke eksisterer så skal man bygge de forskellige docker files via:

```bash
docker build -t <navn her> .
```

Efter dette, så kan man så lave sin swarm.

Til at starte med, skal ens enhed gøres klar til swarm, hvilket sker ved følgende command:

```bash
docker swarm init
```

Derefter så skal man deploy vores docker-compose.yaml til swarm. Dette sker således:

```bash
docker stack deploy -c docker-compose.yml <navn på stack>
```

```
Creating network examStack_default
Creating service examStack_db
Creating service examStack_frontend
Creating service examStack_backend
```

For så at se status på ens stack, kan man sige:

```bash
docker service ls
```

```
ID             NAME                 MODE         REPLICAS   IMAGE                  PORTS
nupkezd2xest   examStack_backend    replicated   3/3        exam_backend:latest    *:3000->3000/tcp
44m84zh4b8kd   examStack_db         replicated   1/1        mariadb:latest
bc1fputrf5x3   examStack_frontend   replicated   3/3        exam_frontend:latest   *:8080->80/tcp
```

Hvis man så vil fjerne ens service fra ens swarm, kan man sige:

```bash
docker stack rm <navn på stack>
```

```
Removing service examStack_backend
Removing service examStack_db
Removing service examStack_frontend
Removing network examStack_default
```