# CL Eksamen 2023e

Dette repository er min løsning for eksamensprojektet i Containerization & Linus 2023.


## Oversigt

Nedenstående viser en oversigt over de mest relevante aspekter af repositoriet.

```
./project
    /backend
        /Dockerfile
    /frontend
        /Dockerfile
        /nginx.conf
    /docker-compose.yaml
./Dokumentation.md
./Synopsis.pdf
```

```Dokumentation.md``` indeholder de nødvendige instruksor for at få projektet hentet ned, og kørt på ens lokale maskine.

```Synopsis.pdf``` er den skriftlige synopsis der skulle afleveres.

```project/backend/Dockerfile``` er den Dockerfile der blev brugt til at lave et image for backend delen af projektet.

```project/frontend/Dockerfile``` er den Dockerfile der blev brugt til at lave et image for frontend delen af projektet.

```project/frontend/``` er en custom konfigurations fil for nginx containeren der hoster frontenden.

```project/docker-compose.yaml``` er selve opsætningen af hele projektet, og angiver hvordan at ens swarm skal køre det.